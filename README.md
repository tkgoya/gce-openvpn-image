MiCloud maked OpenVPN image at GCP install Guide
====

# Package description

This is build for easily and quickly build and openvpn gce instance. This repository include the client ca (ca.crt) and config (config.ovpn) files, that can use for client to connect to the created openvpn instance. Enjoy...

# Image info

* file name: openvpn-standalone.tar.gz
* size: 561069736 byte
* pre-installations: openvpn, node.js, mysql
* description: using local mysql server as the account data store

# Install image

## Pre-require

Please have google cloud sdk first, install reference: https://developers.google.com/cloud/sdk/

## Checkout and import to image repository

```
git clone https://simonsu@bitbucket.org/simonsu/gce-openvpn-image.git
cd gce-openvpn-image
gsutil cp openvpn-standalone.tar.gz gs://[your-bucket-name-and-path]
gcutil addimage [your-image-name] gs://[your-bucket-name-and-path]/openvpn-standalone.tar.gz
```

## Create GCE instance

```
gcutil --service_version="v1" \
	--project="[your-project-id]" addinstance "[instance-name]" \
	--zone="[zone]" --machine_type="[type]" \
	--network="[your-network]" --external_ip_address="ephemeral" \
	--can_ip_forward="true" --tags="https-server" \
	--image="https://www.googleapis.com/compute/v1/projects/[your-project-id]/global/images/[your-openvpn-import-image]" \
	--persistent_boot_disk="true" 
```

Change the following parameters:
* [your-project-id]: Your project id.
* [instance-name]: Your instance name.
* [your-network]: The network that your vpn will create in.
* [zone]: The zone name that list in 'gcutil listzones'.
* [type]: The machine type that list in 'gcutil listmachinetypes'.
* [your-openvpn-import-image]: Your openvpn image name.

## Create routing

GCE routing helps vpn client network traffic can route to the setting instance (or ip) and let openvpn server can routing the network to other servers. In this case, you should set the 10.67.0.0/24 (we build this in the image) for routing.

```
gcutil addroute [route-name] [your-route-network] \
	--next_hop_instance https://www.googleapis.com/compute/v1/projects/[your-project-id]/zones/[zone]/instances/[instance-name] \
	--network [your-network] \
```

* [zone]
Change the following parameters:
* [route-name]: The routing name.
* [your-route-network]: The destination network that openvpn will add, in this image, we add 10.67.0.0/24 to client.
* [your-network]: Your route will create in the network you specific.
* [your-project-id]: Your project id.
* [zone]: Your instance existing zone.
* [instance-name]: The openvpn instance name you create.

After install the image, you can create an openvpn server.

# VPN account configure

Login into instance and use mysql to add the user...

```
$ mysql -uroot -p vpn 
mysql> INSERT INTO `user_info` (`username`, `passwd`, `status`, `create_date`) VALUES ('simonsu', '1234', 1, now());
```

# VPN service start/stop

Start service

```
$ sudo /etc/init.d/openvpn start
```

Stop service

```
$ sudo /etc/init.d/openvpn stop
```

# New issue openvpn RSA key sets (option)

Some times, for security issue... we need to re-generate vpn rsa keys. The instruction bellow will help for generating the key and replace the default keys in image.

## Prepare the easy_rsa tool for generate key set

```
git clone https://github.com/OpenVPN/easy-rsa
```

## Use easy-rsa to generate keys

```
cd easy-rsa/easyrsa3
./easyrsa init-pki
./easyrsa build-ca
(This will request a key phass, and the key is for build server and client key use...)
./easyrsa build-server-full server nopass
./easyrsa build-client-full client nopass
```

This will generate the pki folder and related keys in this folder

## Copy keys to openvpn folder

```
cp pki/ca.crt /etc/openvpn/
cp pki/private/* /etc/openvpn/
cp pki/issued/* /etc/openvpn/
```

## Restart openvpn

```
/etc/init.d/openvpn restart
```

## Update client setting

* Copy ca.crt to client for use

# OpenVPN Client

* Mac: https://code.google.com/p/tunnelblick/
* Windows: https://sites.google.com/a/mitac.com.tw/google-cloud-platform/others/windows-install-openvpn-client

# Trouble shooting

## If your traffic cannot route to other destination: 

You can try to manually execute the following command for fource routing:

```
echo 1 > /proc/sys/net/ipv4/ip_forward
```

## If you want to add or modify openvpn routing

In the config file: server.conf, add the routing to the push block.

```
$ vi /etc/openvpn/server.conf

...(skip)
push "route 10.240.0.0 255.255.0.0 vpn_gateway"
...(skip)

```

## If you want to change your openvpn network

You need to modify the server.conf...

```
$ vi /etc/openvpn/server.conf

...(skip)
server 10.67.0.0 255.255.0.0
...(skip)
```
Find the server block and update to your prefer network.

You need to update the routing table...

```
$ vi /etc/sysconfig/iptables

...(skip)
-A POSTROUTING -s 10.67.0.0/24 -o eth0 -j MASQUERADE
...(skip)

```

Find the POSTROUTING block and update to your prefer network the same as server.conf...


